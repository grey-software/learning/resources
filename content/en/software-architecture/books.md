---
title: Books
description:  We're compiling a library of useful software architecture books from around the Internet to help you on your software journey!
position: 13000
category: Software Architecture
---


## Software Architecture

### Agile Planning: From Ideas to Story Cards 

<book-cover link="https://launchschool.com/books/agile_planning" img-src="https://i.imgur.com/hwl2p6b.jpg" alt="Book cover for Agile Planning: From Ideas to Story Cards "> </book-cover>

In this introduction to Agile Software development, learn the basic techniques that engineers and designers use to take a concept for an application and transform it into a set of requirements that can be built with code! Programmers and software engineers will learn about the context of their craft and about the benefits of Agile planning

<cta-button text="Complete Book" link="https://launchschool.com/books/agile_planning"></cta-button>

### Kanban and Scrum - making the most of both

<book-cover link="https://www.infoq.com/minibooks/kanban-scrum-minibook/" img-src="https://i.imgur.com/VKq0dPr.jpg" alt="Book cover for Kanban and Scrum - making the most of both"> </book-cover>

Scrum and Kanban are two flavours of Agile software development - two deceptively simple but surprisingly powerful approaches to software development. So how do they relate to each other?

The purpose of this book is to clear up the fog, so you can figure out how Kanban and Scrum might be useful in your environment.

Part I illustrates the similarities and differences between Kanban and Scrum, comparing for understanding, not for judgement. There is no such thing as a good or bad tool – just good or bad decisions about when and how to use which tool.

Part II is a case study illustrating how a Scrum-based development organization implemented Kanban in their operations and support teams

<cta-button text="Complete Book" link="https://www.infoq.com/minibooks/kanban-scrum-minibook/"></cta-button>


### Software Engineering for Internet Applications

<book-cover link="http://philip.greenspun.com/seia/" img-src="https://i.imgur.com/Vd5j3QV.jpg" alt="Book cover for Software Engineering for Internet Applications"> </book-cover>

After completing this self-contained course on server-based Internet applications software, students will have the skills to take vague and ambitious specifications and turn them into a system design that can be built and launched in a few months.

They will be able to test prototypes with end-users and refine the application design. They will understand how to meet the challenge of extreme business requirements with automatic code generation and the use of open-source toolkits where appropriate. Students will understand HTTP, HTML, SQL, mobile browsers, VoiceXML, data modeling, page flow and interaction design, server-side scripting, and usability analysis.

<cta-button text="Complete Book" link="http://philip.greenspun.com/seia/"></cta-button>


### Web API Design - Brian Mulloy 

<book-cover link="https://pages.apigee.com/rs/apigee/images/api-design-ebook-2012-03.pdf" img-src="https://i.imgur.com/3CaMsZ0.jpg" alt="Book cover for Web API Design"> </book-cover>

This e-book is a collection of design practices that have been developed in collaboration with some of the leading API teams around the world, as they craft their API strategy through a design workshop that have been conducted at Apigee.


<cta-button text="Complete Book" link="https://pages.apigee.com/rs/apigee/images/api-design-ebook-2012-03.pdf"></cta-button>

</br>

<cta-button text="More Books On Software Architecture" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#software-architecture"></cta-button>


## Reverse Engineering

### iOS App Reverse Engineering 

<book-cover link="https://github.com/iosre/iOSAppReverseEngineering" img-src="https://i.imgur.com/joS97H8.jpg" alt="Book cover for iOS App Reverse Engineering"> </book-cover>

iOS App Reverse Engineering provides a unique view inside the software running on iOSTM, the operating system that powers the Apple iPhone® and iPad®. Within, you will learn what makes up application code and how each component fits into the software ecosystem at large. You will explore the hidden second life your phone leads, wherein it is a full-fledged computer
and software development platform and there is no practical limit to its functionality.

<cta-button text="Complete Book" link="https://github.com/iosre/iOSAppReverseEngineering"></cta-button>

</br>

<cta-button text="More Books On Reverse Engineering" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#reverse-engineering"></cta-button>