---
title: Books
description: We're compiling a library of useful machine learning books around the internet to help you on your software journey!
position: 9002
category: Machine Learning
---

## Machine Learning

### A Brief Introduction to Machine Learning for Engineers 

<book-cover link="https://arxiv.org/pdf/1709.02840.pdf" img-src="https://i.imgur.com/xwq2tmb.png" alt="Book cover for A Brief Introduction to Machine Learning for Engineers"> </book-cover>

This monograph aims at providing an introduction to key concepts, algorithms, and theoretical results in machine learning. The treatment concentrates on probabilistic models for supervised and unsupervised learning problems.

In addition to that it introduces fundamental concepts and algorithms by building on first principles, while also exposing the reader to more advanced topics with extensive pointers to the literature, within a unified notation and mathematical framework.

<cta-button text="Download PDF" link="https://arxiv.org/pdf/1709.02840.pdf"></cta-button>

### Machine Learning from Scratch 

<book-cover link="https://dafriedman97.github.io/mlbook/content/introduction.html" img-src="https://i.imgur.com/kt0Ainb.jpg" alt="Book cover for Machine Learning from Scratch "> </book-cover>

This book covers the building blocks of the most common methods in machine learning. This set of methods is like a toolbox for machine learning engineers. Those entering the field of machine learning should feel comfortable with this toolbox so they have the right tool for a variety of tasks.

Each chapter in this book corresponds to a single machine learning method or group of methods. In other words, each chapter focuses on a single tool within the ML toolbox.

<cta-button text="Complete Book" link="https://dafriedman97.github.io/mlbook/content/introduction.html"></cta-button>

### Understanding Machine Learning: From Theory to Algorithms 

<book-cover link="https://www.cs.huji.ac.il/w~shais/UnderstandingMachineLearning/" img-src="https://i.imgur.com/eMQolgD.jpg" alt="Book cover for  Understanding Machine Learning: From Theory to Algorithms "> </book-cover>

The aim of this textbook is to introduce machine learning, and the algorithmic paradigms it offers, in a principled way. The book provides a theoretical account of the fundamentals underlying machine learning and the mathematical derivations that transform these principles into practical algorithms.

<cta-button text="Complete Book" link="https://www.cs.huji.ac.il/w~shais/UnderstandingMachineLearning/"></cta-button>

</br>

<cta-button text="More Books On Machine Learning" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#machine-learning"></cta-button>

## R

### Advanced R Programming 

<book-cover link="http://adv-r.had.co.nz/" img-src="https://i.imgur.com/6yAQ4xs.jpg" alt="Book cover for Advanced R Programming "> </book-cover>

This book is designed primarily for R users who want to improve their programming skills and understanding of the language and explains some of R’s quirks and shows how some parts that seem horrible do have a positive side.

<cta-button text="Complete Book" link="http://adv-r.had.co.nz/"></cta-button>


### Data Analysis and Prediction Algorithms with R

<book-cover link="https://rafalab.github.io/dsbook/index.html#preface" img-src="https://i.imgur.com/I143iCQ.png" alt="Book cover for Data Analysis and Prediction Algorithms with R"> </book-cover>

The demand for skilled data science practitioners in industry, academia, and government is rapidly growing.

This book introduces concepts from probability, statistical inference, linear regression and machine learning and R programming skills.

<cta-button text="Complete Book" link="https://rafalab.github.io/dsbook/index.html#preface"></cta-button>


### The R Inferno

<book-cover link="https://www.burns-stat.com/pages/Tutor/R_inferno.pdf" img-src="https://i.imgur.com/Cm8qPjE.jpg" alt="Book cover for The R Inferno"> </book-cover>

An essential guide to the trouble spots and oddities of R. In spite of the quirks exposed here, R is the best computing environment for most data analysis tasks.

R is free, open-source, and has thousands of contributed packages. 

<cta-button text="Download PDF" link="https://www.burns-stat.com/pages/Tutor/R_inferno.pdf"></cta-button>

</br>

<cta-button text="More Books On R Programming" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#r"></cta-button>

<cta-button text="More Resources On R Programming" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#r"></cta-button>

## Python

### A Practical Introduction to Python Programming 

<book-cover link="https://www.brianheinold.net/python/A_Practical_Introduction_to_Python_Programming_Heinold.pdf" img-src="https://i.imgur.com/grYl3mz.png" alt="Book cover for A Practical Introduction to Python Programming "> </book-cover>

This book is specifically designed to be used in an introductory programming course, but it is also useful for those with prior programming experience looking to learn Python.

<cta-button text="Download PDF" link="https://www.brianheinold.net/python/A_Practical_Introduction_to_Python_Programming_Heinold.pdf"></cta-button>


### Full Stack Python

<book-cover link="https://www.fullstackpython.com/" img-src="https://i.imgur.com/2sItjcV.jpg" alt="Book cover for Full Stack Python"> </book-cover>

This guide explains important Python concepts in plain language terms without assuming that you already know much about web development, deployments or running an application.

Each chapter focuses on a large subject area, such as data or development environments, then digs into concepts and implementations that you should know to make the right choices for what to use when building your projects.

<cta-button text="Complete Book" link="https://www.fullstackpython.com/"></cta-button>


### Invent Your Own Computer Games With Python

<book-cover link="https://inventwithpython.com/invent4thed/" img-src="https://i.imgur.com/xQ2Wsea.png" alt="Book cover for Invent Your Own Computer Games With Python"> </book-cover>

This guide teaches you how to program in the Python language. Each chapter gives you the complete source code for a new game, and then teaches the programming concepts from the examples.


<cta-button text="Complete Book" link="https://inventwithpython.com/invent4thed/"></cta-button>

</br>

<cta-button text="More Books On Python" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-langs.md#python"></cta-button>

<cta-button text="More Resources On Python" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-interactive-tutorials-en.md#python"></cta-button>

