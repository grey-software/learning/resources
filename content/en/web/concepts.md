---
title: Concepts
description: We're compiling useful resources around the internet to help you understand web .
position: 2502
category: The Web
---

## Web Components

Web components are a set of web platform APIs that allow you to create new custom, reusable, encapsulated HTML tags to use in web pages and web apps

<youtube-video id="83W63gTVlSk"> </youtube-video>


## The DOM

The Document Object Model (DOM) is the data representation of the objects that comprise the structure and content of a document on the web. 

This video will introduce the DOM, look at how the DOM represents an HTML document in memory and how to use APIs to create web content and applications.

<youtube-video id="i_T33FSl254"> </youtube-video>

## package.json

A package.json is a JSON file that exists at the root of a Javascript/Node project. It holds metadata relevant to the project and it is used for managing the project's dependencies, scripts, version and a whole lot more.

<youtube-video id="4xR2nV7MGfE"> </youtube-video>

## Tech Stack

A "tech stack" includes all the technologies used to build a complete web or mobile application - like frameworks, cloud services, libraries, languages, and APIs. 

In this video you will learn how to over-engineer a tech stack for an MVP, then simplify it.

<youtube-video id="Sxxw3qtb3_g"> </youtube-video>

## What are Scalable Vector Graphics (SVG) & how are they special?

In this video, you will learn image formats, especially SVG (Scalable Vector Graphics), the only vector image format for the web. It has unique abilities that let you create awesome graphic effects and it's actually easier to create than you think.

<youtube-video id="hA7ESX7FsE4?list=PLnRGhgZaGeBuCVMW7eNFUQCR6au0jGmcK"> </youtube-video>

## pnpm

PNPM stands for Performant NPM, which means it stands for **Performant Node Package Manager**. It is a drop-in replacement for npm, but faster and more efficient. 

<youtube-video id="hiTmX2dW84E"> </youtube-video>

