---
title: Concepts
description: We're compiling a library of useful Operating Systems concepts from around the Internet to help you on your software journey!
position: 16500
category: Operating Systems
---

## Operating System Overview 

An operating system:
- Makes it easy to use programs.
- Allows multiple programs to run simultaneously without clashes. They share the same memory and their data doesn't get scrambled together.
- Facilitates communication between the computer and devices attached to it.
- Gives the user an easy interface for interacting with the device's hardware.

[![Operating System Overview - tutorialspoint.com](https://i.imgur.com/ut6jOof.png)](https://github.com/ahmaazouzi/cs-topics/blob/master/os/overview.md)

[Source : ahmaazouzi - Github](https://github.com/ahmaazouzi)

<cta-button text="Learn More" link="https://github.com/ahmaazouzi/cs-topics/blob/master/os/overview.md"></cta-button>


## Processes 

Processes are probably the most basic and powerful abstraction provided by operating systems.
In this part, you will learn: 
- What is a process and how an OS uses it.
- Important UNIX system calls, designed specifically for creating and managing processes.
- Direct execution, and how the OS uses it to create efficient processes that it can still control and supervise.

[![Process States in Operating System - Webeduclick.com](https://i.imgur.com/AfSdAtA.png)](https://github.com/ahmaazouzi/cs-topics/blob/master/os/processes.md)

[Source : ahmaazouzi - Github](https://github.com/ahmaazouzi)

<cta-button text="Learn More" link="https://github.com/ahmaazouzi/cs-topics/blob/master/os/processes.md"></cta-button>


## Scheduling 

Scheduling basically refers to the high-level policies used by the OS to manage slices of time it allots to each process.

[![OS scheduling Preview - krivalar.com](https://i.imgur.com/3ilPYbu.png)](https://github.com/ahmaazouzi/cs-topics/blob/master/os/scheduling.md)

[Source : ahmaazouzi - Github](https://github.com/ahmaazouzi)

<cta-button text="Learn More" link="https://github.com/ahmaazouzi/cs-topics/blob/master/os/scheduling.md"></cta-button>