---
title: Contributing Overview
description: Learn about how to contribute resources to this website.
category: Contributing
position: 100
---

Thank you for considering to contribute resources to our website!

With your help, we can build a library of useful online resources for people worldwide to benefit from!

## Prerequisites

- [Learning Markdown is essential for contributing web content on our websites](https://learn.grey.software/markdown)

## Flow Chart

The following flow chart briefly describes how you can contribute!

<flow-chart name="contributing-resources"></flow-chart>

<alert type="info"> Still confused? <br/> Dont worry, we have got you covered with a detailed guide with steps you can follow. We can also help you out on Discord if you get stuck!</alert>

