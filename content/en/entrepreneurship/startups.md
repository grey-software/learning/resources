---
title: Startups
description: We're compiling a library of useful videos about entrepreneurship from around the Internet to help you on your software journey!
position: 1003
category: Entrepreneurship
---

## Startup Launch List

A​rticles you need to read before launching a startup. Written by​ ​founders, designers, investors and thought leaders.

[![Startup Launch List Preview ](https://i.imgur.com/PadtAzI.png)](https://startuplaunchlist.com/)

[Source : startuplaunchlist.com](https://startuplaunchlist.com/)

<cta-button  link="https://startuplaunchlist.com/" text="Read Articles"></cta-button>