---
title: Courses
description:  We're compiling a library of free courses about Math for Computer Science from around the Internet to help you on your software journey!
position: 13500
category: Math for CS
---

## Free Courses

### Computational Linear Algebra for Coders

This course is focused on the question: How do we do matrix computations with acceptable speed and acceptable accuracy?

This course was taught in the University of San Francisco's Masters of Science in Analytics program, summer 2017 (for graduate students studying to become data scientists). 

The course uses: 
- Python with Jupyter Notebooks
- Scikit-Learn
- Numpy
- Numb
- PyTorch

<cta-button text="Full Course" link="https://github.com/fastai/numerical-linear-algebra/blob/master/README.md"></cta-button>


## University Courses

### Mathematics for Computer Science (MIT)

This course covers elementary discrete mathematics for computer science and engineering. It emphasizes mathematical definitions and proofs as well as applicable methods. 

Topics include formal logic notation, proof methods; induction, well-ordering; sets, relations; elementary graph theory; integer congruences; asymptotic notation and growth of functions; permutations and combinations, counting principles; discrete probability. Further selected topics may also be covered, such as recursive definition and structural induction; state machines and invariants; recurrences; generating functions.

<youtube-video id="L3LMbpZIKhQ?list=PLB7540DEDD482705B"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PLB7540DEDD482705B"></cta-button>

### Mathematics of Big Data and Machine Learning (MIT)

This course introduces the Dynamic Distributed Dimensional Data Model (D4M), a breakthrough in computer programming that combines graph theory, linear algebra, and databases to address problems associated with Big Data. Search, social media, ad placement, mapping, tracking, spam filtering, fraud detection, wireless communication, drug discovery, and bioinformatics all attempt to find items of interest in vast quantities of data.

This course teaches a signal processing approach to these problems by combining linear algebraic graph algorithms, group theory, and database design. This approach has been implemented in software.

<youtube-video id="t4K6lney7Zw?list=PLUl4u3cNGP62uI_DWNdWoIMsgPcLGOx-V"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/playlist?list=PLUl4u3cNGP62uI_DWNdWoIMsgPcLGOx-V"></cta-button>


