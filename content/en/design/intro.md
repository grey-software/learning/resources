---
title: Intro
description: We're compiling useful resources around the internet to help you learn digital design.
position: 1100
category: Design
---

## How To Design by Gary Tan!

This video will help to start designing anything, anywhere, regardless of whether or not you've ever done it before.

<youtube-video id="K3Ctc1UN0bQ"></youtube-video>

