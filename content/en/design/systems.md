---
title: Design Systems
description: We're compiling design systems from around the internet to help you on your next project!
position: 1116
category: Design 
---

![](https://i.imgur.com/DU7PIOV.jpg)

A Design system is an organized set of interconnected patterns and shared practices.

Design systems aid in digital product design and development of products such as applications or websites.

They may contain, but are not limited to, pattern libraries, design languages, style guides, coded components, brand languages, and documentation.

[Source: Wikipedia](https://en.wikipedia.org/wiki/Design_system) 

<cta-button text="Our Design System" link="figma.grey.software"></cta-button> <cta-button text="More Design Systems" link="https://www.designsystemsforfigma.com/"></cta-button>


- [AirBnb](https://airbnb.design/building-a-visual-language/): Behind the scenes of our new design system.
- [Atlassian](https://atlassian.design/): Use Atlassian’s end-to-end design language to create simple, intuitive and beautiful experiences.
- [Evergreen](https://evergreen.segment.com/): Evergreen is a React UI Framework for building ambitious products on the web. Brought to you by Segment.
- [Fluent](https://www.microsoft.com/design/fluent/): Fluent is an open-source, cross-platform design system that gives designers and developers the frameworks they need to create engaging product experiences—accessibility, internationalization, and performance included. *Microsoft*
- [Carbon](https://www.ibm.com/design/language/): This is the guiding ethos behind IBM’s design philosophy and principles. This helps us distinguish every element and every experience Designed by IBM. *IBM*
- [Human Interface Guidelines](https://developer.apple.com/design/human-interface-guidelines/): Get in-depth information and UI resources for designing great apps that integrate seamlessly with Apple platforms. *Apple*
- [Lightning Design System](https://www.lightningdesignsystem.com/): Salesforce Lightning Design System. Create the world’s best enterprise app experiences. *Salesforce*
- [Mailchimp](https://ux.mailchimp.com/patterns/): Mailchimp Design System
- [Material Design](https://material.io/): Material is a design system – backed by open-source code – that helps teams build high-quality digital experiences. *Google*
- [Polaris](https://polaris.shopify.com/): Our design system helps us work together to build a great experience for all of Shopify’s merchants. *Shopify*
- [Primer](https://primer.style/): Primer was created for GitHub by GitHub. We love it so much, we chose to open-source it to allow the community to design and build their own projects with Primer. *Github*
- [Solid](https://solid.buzzfeed.com/): Solid is BuzzFeed's CSS style guide. Influenced by frameworks like Basscss, Solid uses immutable, atomic CSS classes to rapidly prototype and develop features, providing consistent styling options along with the flexibility to create new layouts and designs without the need to write additional CSS. *BuzzFeed*
- [Style Guides](http://styleguides.io/): Real life pattern libraries, code standards documents and content style guides.
- [Uber](https://brand.uber.com/): These guidelines cover 9 elements: logo, color, composition, iconography, illustration, motion, photography, tone of voice, and typography.
- [Cookbook](https://www.yelp.com/styleguide/): The styleguide is a resource for designers, product managers, and developers, providing a common language around Yelp’s UI patterns. 




## ANT Design System

![Preview Image For ANT Design System](https://s3-alpha.figma.com/hub/file/2336661714/3caed5c2-059b-4bbc-bd32-ef3709881fec-cover.png)

<cta-button text="Get a Copy" link="https://www.figma.com/community/file/831698976089873405"></cta-button>

## UI2: Figma's Design System

![Preview Image for Figma's Design System](https://i.imgur.com/Z10Pnwo.png)

<cta-button text="Get a Copy" link="https://www.figma.com/community/file/928108847914589057"></cta-button>

## Base Gallery: Uber's Design System

![Preview Image for Uber's Design System](https://i.imgur.com/oDWPYeH.png)

<cta-button text="Get a Copy" link="https://www.figma.com/community/file/805195278314519508"></cta-button>


