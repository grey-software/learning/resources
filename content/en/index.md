---
title: Intro
description: We've compiled a library of useful resources from around the Internet to help you on your software journey!
position: 1
category: Overview
---

### Welcome to Grey Software's resources website!

We built this website to curate and crowd-source the most up-to-date software resources from professionals and academics worldwide.

We're adding new resources to this website every week, so be sure to check back in or follow our socials if you want to stay updated!

### Contributing

If you have some resources you'd like to share, check out [our contributing guide](/contributing) to learn how!
