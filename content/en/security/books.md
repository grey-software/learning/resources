---
title: Cyber Security Books
description: We're compiling useful security books around the internet to help you learn cybersecurity!
position: 8001
category: Cyber Security 
---

### A Graduate Course in Applied Cryptography

<book-cover link="http://toc.cryptobook.us/" img-src="https://i.imgur.com/kNiStHv.png" alt="Book cover for A Graduate Course in Applied Cryptography"> </book-cover>

**Author: D. Boneh and V. Shoup**

This book is about mathematical modeling and proofs to show that a particular cryptosystem satisfies the security properties attributed to it: constructing practical cryptosystems to enhance security under plausible assumptions.


<cta-button text="Download PDF" link="http://toc.cryptobook.us/"></cta-button>


### Handbook of Applied Cryptography

<book-cover link="https://cacr.uwaterloo.ca/hac/index.html" img-src="https://i.imgur.com/NyIKevz.jpg" alt="Book cover for Handbook of Applied Cryptography"> </book-cover>

**Author: Alfred J. Menezes, Paul C. van Oorschot and Scott A. Vanstone**

This Handbook will serve as a valuable reference for the novice as well as for the expert who needs a wider scope of coverage within the area of cryptography. It is a necessary and timely guide for professionals who practice the art of cryptography.

<cta-button text="Complete Book" link="https://cacr.uwaterloo.ca/hac/index.html"></cta-button>


### Security Engineering

<book-cover link="https://www.cl.cam.ac.uk/~rja14/book.html" img-src="https://i.imgur.com/rj1YDbM.jpg" alt="Book cover for Security Engineering"> </book-cover>

**Author: R. Anderson**

This book is about building dependable Distributed Systems and teaches how to design, implement, and test systems to withstand both error and attack

<cta-button text="Complete Book" link="https://www.cl.cam.ac.uk/~rja14/book.html"></cta-button>

</br>

<cta-button text="More Books On Cyber Security" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#security"></cta-button>