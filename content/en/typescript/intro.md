---
title: Introduction
description: We're curating useful Typescript resources from around the Internet to help you on your software journey!
position: 18500
category: TypeScript
---

## Overview

TypeScript is a programming language developed and maintained by Microsoft. It is a strict syntactical superset of JavaScript and adds optional static typing to the language.

TypeScript is used to develop JavaScript applications for both client-side and server-side execution (as with Node. js or Deno).

The goal of TypeScript is to help catch mistakes early through a type system and to make JavaScript development more efficient.

<youtube-video id="zQnBQ4tB3ZA"> </youtube-video>

## Typescript Basics

<youtube-video id="ahCwqrYpIuM"> </youtube-video>
