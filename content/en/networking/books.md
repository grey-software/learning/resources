---
title: Books
description: We're compiling a library of useful networking books from around the Internet to help you on your software journey!
position: 17001
category: Networking
---

## Networking

### An Introduction to Computer Networks

<book-cover link="http://intronetworks.cs.luc.edu/" img-src="https://i.imgur.com/U1NE77g.png" alt="Book cover for An Introduction to Computer Networks"> </book-cover>

An Introduction to Computer Networksis a free and open general-purpose computer-networking textbook, complete with diagrams and exercises.It covers the LAN, internetworking and transport layers, focusing primarily on TCP/IP. Particular attention is paid to congestion; other special topics include queuing, real-time traffic, network management, security and the ns simulator.

The book is suitable as the primary text for an undergraduate or introductory graduate course in computer networking, as a supplemental text for a wide variety of network-related courses, and as a reference work

<cta-button text="Complete Book" link="http://intronetworks.cs.luc.edu/"></cta-button>


### Computer Networking : Principles, Protocols and Practice 

<book-cover link="https://www.computer-networking.info/1st/html/index.html" img-src="https://i.imgur.com/0te7BiX.png" alt="Book cover for Computer Networking : Principles, Protocols and Practice"> </book-cover>

This is an ongoing effort to develop an open-source networking textbook that could be used for an in-depth undergraduate or graduate networking courses.

Building on the successful top-down approach, it continues with an early emphasis on application-layer paradigms and application programming interfaces, encouraging a hands-on experience with protocols and networking concepts.

Networking today involves much more than standards specifying message formats and protocol behaviors - and it is far more interesting. This book focus on describing emerging principles in a lively and engaging manner and then illustrate these principles with examples drawn from Internet architecture.

<cta-button text="Complete Book" link="https://www.computer-networking.info/1st/html/index.html"></cta-button>


### High-Performance Browser Networking

<book-cover link="https://hpbn.co/" img-src="https://i.imgur.com/ArcWE4J.jpg" alt="Book cover for "> </book-cover>

Performance is a feature. This book provides a hands-on overview of what every web developer needs to know about the various types of networks (WiFi, 3G/4G), transport protocols (UDP, TCP, and TLS), application protocols (HTTP/1.1, HTTP/2), and APIs available in the browser (XHR, WebSocket, WebRTC, and more) to deliver the best—fast, reliable, and resilient—user experience.

<cta-button text="Complete Book" link="https://hpbn.co/"></cta-button>

</br>

<cta-button text="More Books On Networking" link="https://github.com/EbookFoundation/free-programming-books/blob/main/books/free-programming-books-subjects.md#networking"></cta-button>
