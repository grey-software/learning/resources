---
title: Concepts
description: We're compiling a library of useful networking concepts from around the Internet to help you on your software journey!
position: 17000
category: Networking
---

## A Tour of the Internet and Networking 

The Internet is really a network of networks. That is, the Internet is an interconnected set of privately and publicly owned and managed networks. Any network connected to the Internet must run the IP protocol and conform to certain naming and addressing conventions

[![A Tour of the Internet and Networking Preview](https://i.imgur.com/cnY2UJq.png)](https://github.com/ahmaazouzi/cs-topics/blob/master/networking/networking-and-internet.md)

[Source : ahmaazouzi - Github](https://github.com/ahmaazouzi)

<cta-button text="Learn More" link="https://github.com/ahmaazouzi/cs-topics/blob/master/networking/networking-and-internet.md"></cta-button>


## The Application Layer

The application layer is confined to end systems. Core devices such as switches and routers don't understand anything about application and are confined to the 3 bottom layers of our 5-layer Internet stack!

[![The Application Layer Preview](https://i.imgur.com/X9gGuEW.png)](https://github.com/ahmaazouzi/cs-topics/blob/master/networking/application.md)

[Source : ahmaazouzi - Github](https://github.com/ahmaazouzi)

<cta-button text="Learn More" link="https://github.com/ahmaazouzi/cs-topics/blob/master/networking/application.md"></cta-button>


## The Transport Layer

Transport layer is responsible for transmitting data between different processes in the layered network architecture. You will learn about the principles of transport and how these are implemented in actual protocols (specifically UDP and TCP).

[![The Transport Layer Preview](https://i.imgur.com/X9gGuEW.png)](https://github.com/ahmaazouzi/cs-topics/blob/master/networking/network.md)

[Source : ahmaazouzi - Github](https://github.com/ahmaazouzi)

<cta-button text="Learn More" link="https://github.com/ahmaazouzi/cs-topics/blob/master/networking/network.md"></cta-button>


## The Network Layer 

The process-to-process communication in the transport layer depends on services provided by the layer under it, the **network layer** which is responsible for host-to-host communication. This is probably the most important and interesting layer in the networking stack. While the application and transport layers are implemented only in the end systems, the network layer is implemented across the whole network, so every router in the network has to process it.

[![The Network Layer Preview](https://i.imgur.com/mc82KC8.png)](https://github.com/ahmaazouzi/cs-topics/blob/master/networking/network.md)

[Source : ahmaazouzi - Github](https://github.com/ahmaazouzi)

<cta-button text="Learn More" link="https://github.com/ahmaazouzi/cs-topics/blob/master/networking/network.md"></cta-button>


## The Link Layer

In computer networking, the link layer is the lowest layer in the Internet protocol suite, the networking architecture of the Internet. The link layer is the group of methods and communications protocols confined to the link that a host is physically connected to.

[![The Link Layer Preview](https://i.imgur.com/XeJZBVw.png)](https://github.com/ahmaazouzi/cs-topics/blob/master/networking/link.md)

[Source : ahmaazouzi - Github](https://github.com/ahmaazouzi)

<cta-button text="Learn More" link="https://github.com/ahmaazouzi/cs-topics/blob/master/networking/link.md"></cta-button>


## Wireless and Mobile Networks

In this part, you will learn about the wireless networking and the issue of mobility and how wireless networking relates and compares to traditional wired networking

[![Wireless and Mobile Networks Preview](https://i.imgur.com/UOqiMFA.png)](https://github.com/ahmaazouzi/cs-topics/blob/master/networking/wireless.md)

[Source : ahmaazouzi - Github](https://github.com/ahmaazouzi)

<cta-button text="Learn More" link="https://github.com/ahmaazouzi/cs-topics/blob/master/networking/wireless.md"></cta-button>


## Multimedia Networking

We all want to know how to create a video streaming service or a live video chatting application like WhatApp!! This part will cover the networking issues of transmitting sound and video through the network.

[![Multimedia Networking Preview](https://i.imgur.com/uQKo9oy.png)](https://github.com/ahmaazouzi/cs-topics/blob/master/networking/multimedia.md)

[Source : ahmaazouzi - Github](https://github.com/ahmaazouzi)

<cta-button text="Learn More" link="https://github.com/ahmaazouzi/cs-topics/blob/master/networking/multimedia.md"></cta-button>


## Security in Computer Networks


This part will be mainly about secure communication over IP and how to defend it against various types of attacks

[![Security in Computer Networks Preview](https://i.imgur.com/ymvifon.png)](https://github.com/ahmaazouzi/cs-topics/blob/master/networking/security.md)

[Source : ahmaazouzi - Github](https://github.com/ahmaazouzi)

<cta-button text="Learn More" link="https://github.com/ahmaazouzi/cs-topics/blob/master/networking/security.md"></cta-button>