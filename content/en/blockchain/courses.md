---
title: Courses
description: We're compiling useful blockchain courses from top universities to help you on your software journey!
position: 9500
category: Blockhain
---

## Blockchain and Money (MIT)

This course is for students wishing to explore blockchain technology's potential use—by entrepreneurs and incumbents—to change the world of money and finance. 

The course begins with a review of Bitcoin and an understanding of the commercial, technical, and public policy fundamentals of blockchain technology, distributed ledgers, and smart contracts. The class then continues on to current and potential blockchain applications in the financial sector. 

<youtube-video id="EH6vE97qIP4?list=PLUl4u3cNGP63UUkfL0onkxF6MYgVa04Fn"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/watch?list=PLUl4u3cNGP63UUkfL0onkxF6MYgVa04Fn&v=EH6vE97qIP4"></cta-button>

