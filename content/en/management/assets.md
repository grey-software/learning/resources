---
title: Assets
description: We're compiling a library of useful Project Management assets from around the Internet to help you on your software journey!
position: 4003
category: Product Management
---

## Awesome Product Management Resources 

A curated list of awesome resources for product/program managers to learn and grow.

[![Awesome Product Management Preview](https://i.imgur.com/NUrHuiM.png)](https://github.com/dend/awesome-product-management)

[Source : Den Delimarsky -  Github](https://github.com/dend)

<cta-button text="Resources" link="https://github.com/dend/awesome-product-management"></cta-button>