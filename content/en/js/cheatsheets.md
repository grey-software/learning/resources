---
title: Cheat Sheets
description: We're curating useful JavaScript cheatsheets from around the Internet to help you on your software journey!
position: 3008
category: Javascript
---

## Javascript 

### Modern JavaScript Cheatsheet

[![Modern JavaScript Cheatsheet Preview](https://i.imgur.com/NMlqyUY.png)](https://github.com/mbeaudru/modern-js-cheatsheet)

[Source : mbeaudru from Github](https://github.com/mbeaudru)

<cta-button text="View" link="https://github.com/mbeaudru/modern-js-cheatsheet"></cta-button>


### Learn JavaScript in Y minutes

[![Learn JavaScript in Y minutes Preview](https://i.imgur.com/pP4mApd.png)](https://learnxinyminutes.com/docs/javascript/)

[Source : learnxinyminutes.com](https://learnxinyminutes.com/)

<cta-button text="View" link="https://learnxinyminutes.com/docs/javascript/"></cta-button>


### JavaScript Regex Cheatsheet

[![JavaScript Regex Cheatsheet Preview](https://i.imgur.com/3QPyuZ5.png)](https://www.debuggex.com/cheatsheet/regex/javascript)

[Source : Debuggex](https://www.debuggex.com/)

<cta-button text="View" link="https://www.debuggex.com/cheatsheet/regex/javascript"></cta-button>


### JavaScript ES6 Cheatsheet

[![JavaScript ES6 Cheatsheet Preview](https://i.imgur.com/XvRG7Zw.png)](https://www.mediafire.com/file/b9o32oaugrno6z4/Js_Es6_Cheat_Sheet.pdf/file)

[Source: codingtute.com](https://codingtute.com/)

<cta-button text="Download PDF" link="https://www.mediafire.com/file/b9o32oaugrno6z4/Js_Es6_Cheat_Sheet.pdf/file"></cta-button>


### JavaScript API calls Cheatsheet

[![JavaScript API calls Cheatsheet Preview](https://i.imgur.com/ahrOguO.png)](https://www.mediafire.com/file/8se4ljcxwu1toup/Js_API_calls.pdf/file)

[Source: codingtute.com](https://codingtute.com/)

<cta-button text="Download PDF" link="https://www.mediafire.com/file/8se4ljcxwu1toup/Js_API_calls.pdf/file"></cta-button>


### ES6 HTML DOM CheatSheet

[![ES6 HTML DOM Cheatsheet Preview](https://i.imgur.com/iSFyFZY.png)](https://www.mediafire.com/file/45t6cr8yxfcej4h/ES6_HTML_DOM_-_White.pdf/file)

[Source: codingtute.com](https://codingtute.com/)

<cta-button text="Download PDF" link="https://www.mediafire.com/file/45t6cr8yxfcej4h/ES6_HTML_DOM_-_White.pdf/file"></cta-button>


</br>

<cta-button text="More Cheatsheets On Javascript" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#javascript"></cta-button>


## jQuery 

### jQuery CheatSheet

[![jQuery CheatSheet Preview](https://i.imgur.com/QWsO7vL.png)](https://htmlcheatsheet.com/jquery/)

[Source : HTMLCheatSheet](https://htmlcheatsheet.com/)

<cta-button text="View" link="https://htmlcheatsheet.com/jquery/"></cta-button>

</br>

<cta-button text="More Cheatsheets On jQuery" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#jquery"></cta-button>


## Nest.js

### Nest.js CheatSheet 

[![Nest.js CheatSheet Preview](https://i.imgur.com/BWQHM1X.png)](https://gist.github.com/guiliredu/0aa9e4d338bbeeac369a597e87c9ba46)

[Source: guiliredu from Github](https://gist.github.com/guiliredu)

<cta-button text="View" link="https://gist.github.com/guiliredu/0aa9e4d338bbeeac369a597e87c9ba46"></cta-button>

</br>

<cta-button text="More cheatsheets On Nest.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#nestjs"></cta-button>


## Nuxt.js

### Nuxt.js Essentials Cheatsheet 

[![Nuxt.js Essentials Cheatsheet Preview](https://i.imgur.com/CYIDXbB.png)](https://www.vuemastery.com/pdf/Nuxtjs-Cheat-Sheet.pdf)

[Source: VueMastery](https://www.vuemastery.com/)

<cta-button text="View" link="https://www.vuemastery.com/pdf/Nuxtjs-Cheat-Sheet.pdf"></cta-button>

</br>

<cta-button text="More Cheatsheets On Nuxt.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#nuxtjs"></cta-button>


## React.js

### React Cheatsheet 

[![React Cheatsheet Preview](https://i.imgur.com/opqHZJx.png)](https://www.codecademy.com/learn/react-101/modules/react-101-jsx-u/cheatsheet)

[Source : Codecademy](https://www.codecademy.com/)

<cta-button text="View" link="https://www.codecademy.com/learn/react-101/modules/react-101-jsx-u/cheatsheet"></cta-button>

</br>

<cta-button text="More Cheatsheets On React.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#reactjs"></cta-button>


## Vue.js

### Vue Essential Cheatsheet 

[![Vue Essential Cheatsheet Preview](https://i.imgur.com/5UtdOZp.png)](https://www.vuemastery.com/pdf/Vue-Essentials-Cheat-Sheet.pdf)

[Source: VueMastery](https://www.vuemastery.com/)

<cta-button text="View" link="https://www.vuemastery.com/pdf/Vue-Essentials-Cheat-Sheet.pdf"></cta-button>


### Vue.js cheatsheet

[![Vue.js cheatsheet Preview](https://i.imgur.com/Mi9qblk.png)](https://devhints.io/vue)

[Source: devhints.io](https://devhints.io/)

<cta-button text="View" link="https://devhints.io/vue"></cta-button>

### Anatomy of Vue Component

[![Vue cheat sheet Preview](https://i.imgur.com/tE8XNbq.png)](https://github.com/dekadentno/vue-cheat-sheet)

[Source: dekadentno from github](https://github.com/dekadentno)

<cta-button text="View" link="https://github.com/dekadentno/vue-cheat-sheet"></cta-button>

</br>

<cta-button text="More Cheatsheets On Vue.js" link="https://github.com/EbookFoundation/free-programming-books/blob/main/more/free-programming-cheatsheets.md#vuejs"></cta-button>


