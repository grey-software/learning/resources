---
title: Free Courses
description: We're curating useful JavaScript courses from around the Internet to help you on your software journey!
position: 3009
category: Javascript
---


## Next.js Crash Course 2021

In this video you will learn the fundamentals of Next.js such as SSR & SSG, routing, data fetching, apis and more.

<youtube-video id="mTz0GXj8NN0"> </youtube-video>

