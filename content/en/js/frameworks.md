---
title: Frameworks
description: We're curating useful resources about Javascript frameworks from around the Internet to help you on your software journey!
position: 3002
category: Javascript
---

## Vue.js

Vue.js is an open-source Javascript framework used to develop interactive web interfaces.

The creator of Vue.js envisioned a library that brought the best of React and Angular together in a framework that could be progressively adopted.

This video wil help you learn the basics of Vue and build your first reactive UI component.

<youtube-video id="nhBVL41-_Cw?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>

## Angular

Angular is a platform and framework for building single-page client applications using HTML and TypeScript. Angular is written in TypeScript.

It implements core and optional functionality as a set of TypeScript libraries that you import into your applications.

<youtube-video id="Ata9cSC2WpM?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>

## Next.js

Next.js is a [React](https://www.youtube.com/watch?v=Tn6-PIqc4UM&list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN&index=30) framework that enables several extra features, including server-side rendering and generating static websites.

<youtube-video id="Sklc_fQBmcs?list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN"> </youtube-video>

## React Native

React Native is a framework for building native apps using [React](https://www.youtube.com/watch?v=Tn6-PIqc4UM&list=PL0vfts4VzfNiI1BsIK5u7LpPaIDKMJIDN&index=30).

It allows developers to build cross-platform apps for iOS, Android, and the Web from a single JavaScript codebase.

<youtube-video id="gvkqT_Uoahw"> </youtube-video>

## Svelte

Svelte is a JS framework for building reactive UI components. But unlike React, Angular, and Vue, it does not ship a runtime like Virtual DOM to the browser.

<youtube-video id="rv3Yq-B8qp4"> </youtube-video>

### Svelte 3 QuickStart Tutorial

This video will help you learn some basic concepts in Svelte 3 and how to build reactive UI components with JavaScript.

<youtube-video id="043h4ugAj4c"> </youtube-video>
