---
title: Videos
description: We're compiling a library of useful python videos from around the Internet to help you on your software journey!
position: 20500
category: Python 
---

## Python

Python is arguably the world's most popular programming language. It is easy to learn, yet suitable in professional software like web applications, data science, and server-side scripts.

<youtube-video id="x7X9w_GIm1s"> </youtube-video>