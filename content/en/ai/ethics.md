---
title: AI Ethics
description: We're compiling a library of useful AI resources from around the Internet to help you on your software journey!
position: 5503
category: Artificial Intelligence
---

## Data Ethics

Data ethics covers an incredibly broad range of topics, many of which are urgent, making headlines daily, and causing harm to real people right now.

This course provides useful context about how data misuse is impacting society, as well as practice in critical thinking skills and questions to ask.

<cta-button text="Full Course" link="https://ethics.fast.ai/"></cta-button>

## Open Dialogue on AI Ethics

UNESCO has launched a global consultation leading to the adoption of the first global standard-setting instrument on the ethics of artificial intelligence (AI).

A draft recommendation has been elaborated by an ad hoc expert group (AHEG) appointed by UNESCO.

<cta-button text="Learn More" link="https://opendialogueonai.com/"></cta-button>
