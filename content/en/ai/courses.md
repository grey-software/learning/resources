---
title: Courses
description: We're compiling a library of AI courses from around the Internet to help you on your software journey!
position: 5504
category: Artificial Intelligence
---


## Artificial Intelligence (MIT)

This course introduces students to the basic knowledge representation, problem solving, and learning methods of AI.

After completing this course, students should be able to:

- Develop intelligent systems by assembling solutions to concrete computational problems
- Understand the role of knowledge representation, problem solving, and learning in intelligent-system engineering 
- Appreciate the role of problem solving, vision, and language in understanding human intelligence from a computational perspective

<youtube-video id="TjZBTDzGeGg?list=PLUl4u3cNGP63gFHB6xb-kVBiQHYe_4hSi"> </youtube-video>

<cta-button text="Full Playlist" link="https://www.youtube.com/watch?list=PLUl4u3cNGP63gFHB6xb-kVBiQHYe_4hSi&v=TjZBTDzGeGg"></cta-button>



