---
title: Videos
description: We're compiling a library of useful project management videos from around the Internet to help you on your software journey!
position: 19001
category: Project Management 
---

## Project Management - How To Break Down Projects

In this video, Ben and Matthew go in depth into the project management system by breaking down a big project into small, bite-sized tasks. 

Projects break down into phases. Phases break down into tasks. Tasks are completed in a weekly sprint. And milestones are checked off and locked until delivery.

This project management system works for both big teams as well as individuals.

<youtube-video id="zlnVc1nBTto"> </youtube-video>

